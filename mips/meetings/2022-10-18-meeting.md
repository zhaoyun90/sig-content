# October.18, 2022 at 10:00am GMT+8

## Agenda
|时间|议题|发言人|
|--|--|--|
|10:00-10:10|Mips最小集适配进展同步|袁祥仁 李兵|
|10:10-10:20|新建君正芯片孵化仓|袁祥仁 刘佳科|
|10:20-10:30|编译框架适配遗留问题同步|袁祥仁 Lain 刘佳科|
|10:30-10:35|内核适配进展同步|郑曦 袁祥仁 刘佳科|

## Attendees
- [@hongtao6573](https://gitee.com/hongtao6573)
- [@cip_syq](yunqiang.su@oss.cipunited.com)
- [@Lain]()
- [@wicom](https://gitee.com/wicom)
- [@wangxing-hw](https://gitee.com/wangxing-hw)
- [@libing23](https://gitee.com/libing23)
- [@huanghuijin](https://gitee.com/huanghuijin)
- [@huangsox](https://gitee.com/huangsox)
- [@liujk000](https://gitee.com/liujk000)


## Notes

#### 议题一、Mips最小集适配进展同步

**结论**
- X2000开发板可以启动，launcher功能正常，ACE子系统和XTS子系统正在验证功能，下次例会前完成。

#### 议题二、新建君正芯片孵化仓

**结论**
- 因为master分支芯片仓结构变化，君正旧的孵化仓需要改动，下次架构例会前申请新建君正芯片仓。

#### 议题三、编译框架适配遗留问题同步

**结论**
- lld链接器目前存在问题，暂时用bfd，芯联芯已将问题提交到llvm官方仓库，后续继续跟进。
- sysroot目前是预编译的方式，需要参考其他项目改为编译系统时一同编译。

#### 议题四、内核适配进展同步

**结论**
- gcc方式编译内核正常，正在改用clang方式，需和君正沟通确保不含二进制文件。



## Action items
