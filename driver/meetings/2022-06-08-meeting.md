#  June 8, 2022 at 16:00am-17:00am GMT+8

## Agenda

- OH Driver SIG例会[](https://meeting.tencent.com/s/O6GmTrcZcV8a)

  |    时间     | 议题                                 | 发言人                                             |
  | :---------: | ------------------------------------ | ------------------------------------------------- |
  | 16:00-16:10 | 议题一、Media的codec驱动能力开发进展 | 孙赫赫，翟海鹏，刘飞虎 |
  | 16:10-16:20 | 议题二、Display模块新增HDI接口评审 | 赵文华，刘飞虎，袁博，刘洪刚，林洪亮，石鑫栋，翟海鹏，徐飞龙，刘增辉，张云菲 |
  | 16:20-16:40 | 议题三、位置子系统GNSS模块新增HDI接口评审 | 赵文华，刘飞虎，袁博，程国红，向克进，刘彬俊 |
  | 16:40-17:00 | 议题四、AB升级新增HDI接口评审 | 赵文华，刘飞虎，袁博，赖癸仲，张政学，张小田 |

## Notes

- **议题一、Media的codec驱动能力开发进展----孙赫赫，翟海鹏，刘飞虎**

  议题进展：Codec HDI1.0实现根据新的头文件适配，支撑2.0转1.0需求联调，并处理部分1.0遗留问题；Codec HDI2.0编解码均调试ok，已启动与媒体联调；Codec HDI2.0 使用idl生成的C++代码，对接OMX已调试ok，下步准备分批上库。

- **议题二、Display模块新增HDI接口评审----赵文华，刘飞虎，袁博，刘洪刚，林洪亮，石鑫栋，翟海鹏，徐飞龙，刘增辉，张云菲**

  议题结论：对Display模块新增HDI接口和数据类型进行评审，新增3个HDI接口，接口定义符合合规、兼容性和扩展性要求，接口评审通过。

  HDI接口列表：

  | **接口**                                                     | **接口功能**                          |
  | ------------------------------------------------------------ | ------------------------------------- |
  | int32_t (*SetLayerTunnelHandle)(uint32_t devId，uint32_t layerId, ExtDataHandle*handle) | 根据设备和图层id查询ExtDataHandle信息 |
  | int32_t (*GetLayerReleaseFence)(uint32_t devId，uint32_t layerId, int32_t *fence) | 获取图层的fence                       |
  | int32_t (*MmapYUV)(BufferHandle *handle, YUVDescInfo *info)  | 获取Buffer高度对齐信息                |
  | CompositionType::COMPOSITION_DEVICE_CLEAR,CompositionType::COMPOSITION_CLIENT_CLEAR,PixelFormat::PIXEL_FMT_VENDOR_MASK。 | 新增vo视频合成方式枚举                |

- **议题三、位置子系统GNSS模块新增HDI接口评审----赵文华，刘飞虎，袁博，程国红，向克进，刘彬俊**

  议题结论：对位置子系统GNSS模块新增HDI接口和数据类型进行评审，新增31个HDI接口，接口定义符合合规、兼容性和扩展性要求，接口评审通过。

  HDI接口列表：

  https://gitee.com/openharmony/drivers_interface/pulls/85/files

  遗留问题:

  1、位置子系统GNSS新增接口需与法务部余甜评审，并给法务评审结论。---责任人：刘彬俊

- **议题四、AB升级新增HDI接口评审----赵文华，刘飞虎，袁博，赖癸仲，张政学，张小田**

  议题结论：对AB升级模块新增HDI接口和数据类型进行评审，新增4个HDI接口，接口定义符合合规、兼容性和扩展性要求，接口评审通过。

  HDI接口列表：

  https://gitee.com/openharmony/drivers_interface/pulls/87/files

  

 会议通知：https://lists.openatom.io/hyperkitty/list/sig_driver@openharmony.io/thread/3TVVBT4EHJPBHVNGDSTRIBYYHFTKUIMP/

 会议议题申报：https://shimo.im/sheets/36GKhpvrXd8TcQHY/AdiEd

 会议归档：https://gitee.com/openharmony-sig/sig-content/tree/master/driver/meetings


  ## Action items
