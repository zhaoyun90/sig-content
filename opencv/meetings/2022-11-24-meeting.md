
## Agenda

- OH opencv SIG例会

  |    时间     | 议题                                                     | 发言人                 |
  | :---------: | -------------------------------------------------------- | ---------------------- |
  | 9:30-10:30 | 议题一、opencv-sig上次PMC遗留问题反馈及现有方案问题确认   | 王肖云                 |


## Attendees

- [@melody_wxy](https://gitee.com/melody_wxy)
- [@zhangshouzhong](https://gitee.com/zhangshouzhong)


## Notes

#### 议题一、opencv-sig上次PMC遗留问题反馈及现有方案问题确认

**结论**
- 1.opencv SDK包的方式采用灵活的组合方式，并提供详细的指导文档。
- 2.JS同步接口follow社区接口定义，对于部分处理时间较长的接口新增异步接口及实现，接口声明分两个不同的ets文件，用不同的命名空间区分同步和异步接口。
- 3.窗口呈现层参考opencv社区适配其它平台的处理方式，增加pixelMap与OpencvMat间的转换接口。  

**遗留问题**
- 1.opencv-sig现有方案中涉及框架层和服务层的存放位置与OpenHarmony架构不符，后续进行分布式能力开发时再确认具体方案。
  
  ## Action items



