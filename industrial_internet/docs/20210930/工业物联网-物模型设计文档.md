# 物模型

更新时间：2021-9-25

## 什么是物模型

**功能说明**

物模型是物理空间中的实体（如传感器、车载装置、楼宇、工厂等）在云端的数字化表示，从属性、服务和事件三个维度，分别描述了该实体是什么、能做什么、可以对外提供哪些信息。定义了物模型的这三个维度，即完成了产品功能的定义。

| 功能类型        | 说明                                                         |
| --------------- | ------------------------------------------------------------ |
| 属性 (property) | 设备可读取和设置的能力。一般用于描述设备运行时的状态，如环境监测设备所读取的当前环境温度等。属性支持GET和SET请求方式。应用系统可发起对属性的读取和设置请求。 |
| 命令 (command)  | 设备可被外部调用的能力或方法，可设置输入参数和输出参数。产品提供了什么功能供云端调用。相比于属性，服务可通过一条指令实现更复杂的业务逻辑，如执行某项特定的任务。 |
| 事件 (event)    | 设备运行时，主动上报给云端的事件。事件一般包含需要被外部感知和处理的通知信息，可包含多个输出参数。例如，某项任务完成的信息，或者设备发生故障或告警时的温度等，事件可以被订阅和推送。 |

物联网平台支持为产品定义多组功能（属性、命令和事件）。一组功能定义的集合，就是一个物模型模块。多个物模型模块，彼此互不影响。

物模型模块功能，解决了工业场景中复杂的设备建模，便于在同一产品下，开发不同功能的设备。

例如，电暖扇产品的功能属性有电源开关、档位（高、中、低）和室内温度，您可以在一个模块添加前2个属性，在另一个模块添加3个属性，然后分别在不同设备端，针对不同物模型模块功能进行开发。此时，该产品下不同设备就可以实现不同功能。

物联网平台已提供默认模块，您可根据需要新增自定义模块，总模块数量不超过10个。



## 物模型TSL字段说明

物联网平台通过定义一种物的描述语言来描述物模型模块和功能，称为TSL（Thing Specification Language）。物模型TSL文件格式为JSON。

以下是物模型TSL文件中JSON字段及其详细说明。为了完整展示TSL的结构，以下示例中包含所有参数，不代表实际使用中可能出现的组合。参数后的文字为参数说明，非参数值。各参数的使用场景，请参见参数说明。

```javascript
{
    "profile": {
      	"version": "物模型版本号"
        "productKey": "当前产品的ProductKey"
    },
    “specs”: {
      	//物模型扩展信息，比如子设备的协议为opc modbus等协议的连接扩展信息
    }
    "properties": [
        {
            "pid": "属性唯一标识符（产品下唯一）",
            "name": "属性名称",
            "accessMode": "属性读写类型：只读（r）或读写（rw）。",
            "required": "是否是标准功能的必选属性",
            "dataType": {
                "type": "属性类型: int（原生）、float（原生）、double（原生）、text（原生）、date（String类型UTC毫秒）、bool（0或1的int类型）、enum（int类型，枚举项定义方法与bool类型定义0和1的值方法相同）、struct（结构体类型，可包含前面7种类型，下面使用"specs":[{}]描述包含的对象）、array（数组类型，支持int、double、float、text、struct）",
                "specs": {
                    "min": "参数最小值（int、float、double类型特有）",
                    "max": "参数最大值（int、float、double类型特有）",
                    "unit": "属性单位（int、float、double类型特有，非必填）",
                    "unitName": "单位名称（int、float、double类型特有，非必填）",
                    "size": "数组元素的个数，最大512（array类型特有）。",
                    "step": "步长（text、enum类型无此参数）",
                    "length": "数据长度，最大10240（text类型特有）。",
                    "0": "0的值（bool类型特有）",
                    "1": "1的值（bool类型特有）",
                    "item": {
                        "type": "数组元素的类型（array类型特有）"
                    }
                }
            }
        }
    ],
    "events": [
        {
            "pid": "事件唯一标识符（产品下唯一，其中propertyReport是默认生成的属性上报事件。）",
            "name": "事件名称",
            "desc": "事件描述",
            "type": "事件类型（info、alert、error）",
            "required": "是否是标准功能的必选事件",
            "outputData": [
                {
                    "pid": "参数唯一标识符",
                    "name": "参数名称",
                    "dataType": {
                        "type": "属性类型: int（原生）、float（原生）、double（原生）、text（原生）、date（String类型UTC毫秒）、bool（0或1的int类型）、enum（int类型，枚举项定义方法与bool类型定义0和1的值方法相同）、struct（结构体类型，可包含前面7种类型，下面使用'specs':[{}]描述包含的对象）、array（数组类型，支持int、double、float、text、struct）",
                        "specs": {
                            "min": "参数最小值（int、float、double类型特有）",
                            "max": "参数最大值（int、float、double类型特有）",
                            "unit": "属性单位（int、float、double类型特有，非必填）",
                            "unitName": "单位名称（int、float、double类型特有，非必填）",
                            "size": "数组元素的个数，最大512（array类型特有）。",
                            "step": "步长（text、enum类型无此参数）",
                            "length": "数据长度，最大10240（text类型特有）。",
                            "0": "0的值（bool类型特有）",
                            "1": "1的值（bool类型特有）",
                            "item": {
                                "type": "数组元素的类型（array类型特有）"
                            }
                        }
                    }
                }
            ],
            "method": "事件对应的方法名称（根据pid生成）"
        }
    ],
    "commands": [
        {
            "pid": "命令唯一标识符（产品下唯一，其中setProperty/getProperty是根据属性的accessMode默认生成的命令。）",
            "name": "命令名称",
            "desc": "命令描述",
            "required": "是否是标准功能的必选命令",
            "callType": "async（异步调用）或sync（同步调用）",
            "inputData": [
                {
                    "pid": "入参唯一标识符",
                    "name": "入参名称",
                    "dataType": {
                        "type": "属性类型: int（原生）、float（原生）、double（原生）、text（原生）、date（String类型UTC毫秒）、bool（0或1的int类型）、enum（int类型，枚举项定义方法与bool类型定义0和1的值方法相同）、struct（结构体类型，可包含前面7种类型，下面使用'specs':[{}]描述包含的对象）、array（数组类型，支持int、double、float、text、struct）",
                        "specs": {
                            "min": "参数最小值（int、float、double类型特有）",
                            "max": "参数最大值（int、float、double类型特有）",
                            "unit": "属性单位（int、float、double类型特有，非必填）",
                            "unitName": "单位名称（int、float、double类型特有，非必填）",
                            "size": "数组元素的个数，最大512（array类型特有）。",
                            "step": "步长（text、enum类型无此参数）",
                            "length": "数据长度，最大10240（text类型特有）。",
                            "0": "0的值（bool类型特有）",
                            "1": "1的值（bool类型特有）",
                            "item": {
                                "type": "数组元素的类型（array类型特有）"
                                }
                            }
                        }
                    }
                }
            ],
            "outputData": [
                {
                    "pid": "出参唯一标识符",
                    "name": "出参名称",
                    "dataType": {
                        "type": "属性类型: int（原生）、float（原生）、double（原生）、text（原生）、date（String类型UTC毫秒）、bool（0或1的int类型）、enum（int类型，枚举项定义方法与bool类型定义0和1的方法相同）、struct（结构体类型，可包含前面7种类型，下面使用'specs':[{}]描述包含的对象）、array（数组类型，支持int、double、float、text、struct）",
                        "specs": {
                            "min": "参数最小值（int、float、double类型特有）",
                            "max": "参数最大值（int、float、double类型特有）",
                            "unit": "属性单位（int、float、double类型特有，非必填）",
                            "unitName": "单位名称（int、float、double类型特有，非必填）",
                            "size": "数组元素的个数，最大512（array类型特有）。",
                            "step": "步长（text、enum类型无此参数）",
                            "length": "数据长度，最大10240（text类型特有）。",
                            "0": "0的值（bool类型特有）",
                            "1": "1的值（bool类型特有）",
                            "item": {
                                "type": "数组元素的类型（array类型特有）"
                            }
                        }
                    }
                }
            ],
            "method": "命令对应的方法名称（根据pid生成）"
        }
    ],
    //仅自定义模块的TSL中有以下参数。
    "blockId": "自定义模块的唯一标识符，模块ID",
    "blockName": "自定义模块名称"

    //当产品下添加了自定义模块，默认模块TSL中会包含以下参数，表示已添加的自定义模块列表。
    "blocks": [
    {
      "blockId": "自定义模块的唯一标识符，模块ID",
      "blockName": "自定义模块名称",
      "productKey": "产品ProductKey"
    }
  ]
}
```

具体的数据示例：

```javascript
{
  "profile": {
    "version": "1.0",
    "productKey": "a1RcNxQVDUW"
  },
  "properties": [
    {
      "pid": "Status",
      "name": "开关状态",
      "accessMode": "rw",
      "required": false,
      "dataType": {
        "type": "enum",
        "specs": {
          "0": "关闭",
          "1": "打开"
        }
      }
    },
    {
      "pid": "Data",
      "name": "数据",
      "accessMode": "rw",
      "required": false,
      "dataType": {
        "type": "text",
        "specs": {
          "length": "2048"
        }
      }
    },
    {
      "pid": "DeviceInfo",
      "name": "设备属性",
      "accessMode": "rw",
      "desc": "内容推荐",
      "required": false,
      "dataType": {
        "type": "array",
        "specs": {
          "size": "10",
          "item": {
            "type": "text"
          }
        }
      }
    },
    {
      "pid": "Volume",
      "name": "屏幕音量大小",
      "accessMode": "rw",
      "desc": "屏幕音量大小",
      "required": false,
      "dataType": {
        "type": "bool",
        "specs": {
          "0": "大",
          "1": "小"
        }
      }
    }
  ],
  "events": [
    {
      "pid": "propertyReport",
      "name": "propertyReport",
      "type": "info",
      "required": true,
      "desc": "属性上报",
      "method": "event.propertyReport",
      "outputData": [
        {
          "pid": "Status",
          "name": "开关状态",
          "dataType": {
            "type": "enum",
            "specs": {
              "0": "关闭",
              "1": "打开"
            }
          }
        },
        {
          "pid": "Data",
          "name": "数据",
          "dataType": {
            "type": "text",
            "specs": {
              "length": "2048"
            }
          }
        },
        {
          "pid": "DeviceInfo",
          "name": "设备属性",
          "dataType": {
            "type": "array",
            "specs": {
              "size": "10",
              "item": {
                "type": "text"
              }
            }
          }
        },
        {
          "pid": "Volume",
          "name": "屏幕音量大小",
          "dataType": {
            "type": "bool",
            "specs": {
              "0": "大",
              "1": "小"
            }
          }
        }
      ]
    },
    {
      "pid": "UpdateContentCompleteEvent",
      "name": "内容加载完成",
      "type": "info",
      "required": false,
      "method": "event.UpdateContentCompleteEvent.post",
      "outputData": [
        {
          "pid": "UpdateTaskID",
          "name": "下发任务ID",
          "dataType": {
            "type": "text",
            "specs": {
              "length": "128"
            }
          }
        },
        {
          "pid": "Status",
          "name": "状态",
          "dataType": {
            "type": "int",
            "specs": {
              "min": "-1024",
              "max": "1024",
              "unitName": "无",
              "step": "1"
            }
          }
        },
        {
          "pid": "FailedCreativeIDs",
          "name": "失败创意列表",
          "dataType": {
            "type": "array",
            "specs": {
              "size": "128",
              "item": {
                "type": "struct",
                "specs": [
                  {
                    "pid": "CreativeID",
                    "name": "内容创意ID",
                    "dataType": {
                      "type": "text",
                      "specs": {
                        "length": "128"
                      }
                    }
                  },
                  {
                    "pid": "Code",
                    "name": "错误代码",
                    "dataType": {
                      "type": "int",
                      "specs": {
                        "min": "-1024",
                        "max": "1024",
                        "unitName": "无",
                        "step": "1"
                      }
                    }
                  },
                  {
                    "pid": "Message",
                    "name": "错误描述",
                    "dataType": {
                      "type": "text",
                      "specs": {
                        "length": "1024"
                      }
                    }
                  }
                ]
              }
            }
          }
        }
      ]
    }
  ],
  "commands": [
    {
      "pid": "setProperty",
      "name": "setProperty",
      "required": true,
      "callType": "async",
      "desc": "属性设置",
      "method": "command.setProperty",
      "inputData": [
        {
          "pid": "Status",
          "name": "开关状态",
          "dataType": {
            "type": "enum",
            "specs": {
              "0": "关闭",
              "1": "打开"
            }
          }
        },
        {
          "pid": "Data",
          "name": "数据",
          "dataType": {
            "type": "text",
            "specs": {
              "length": "2048"
            }
          }
        },
        {
          "pid": "DeviceInfo",
          "name": "设备属性",
          "dataType": {
            "type": "array",
            "specs": {
              "size": "10",
              "item": {
                "type": "text"
              }
            }
          }
        },
        {
          "pid": "Volume",
          "name": "屏幕音量大小",
          "dataType": {
            "type": "bool",
            "specs": {
              "0": "大",
              "1": "小"
            }
          }
        }
      ],
      "outputData": []
    },
    {
      "pid": "getProperty",
      "name": "getProperty",
      "required": true,
      "callType": "async",
      "desc": "属性获取",
      "method": "command.getProperty",
      "inputData": [
        "Status",
        "Data",
        "DeviceInfo",
        "Volume"
      ],
      "outputData": [
        {
          "pid": "Status",
          "name": "开关状态",
          "dataType": {
            "type": "enum",
            "specs": {
              "0": "关闭",
              "1": "打开"
            }
          }
        },
        {
          "pid": "Data",
          "name": "数据",
          "dataType": {
            "type": "text",
            "specs": {
              "length": "2048"
            }
          }
        },
        {
          "pid": "DeviceInfo",
          "name": "设备属性",
          "dataType": {
            "type": "array",
            "specs": {
              "size": "10",
              "item": {
                "type": "text"
              }
            }
          }
        },
        {
          "pid": "Volume",
          "name": "屏幕音量大小",
          "dataType": {
            "type": "bool",
            "specs": {
              "0": "大",
              "1": "小"
            }
          }
        }
      ]
    }
  ]
}
```

