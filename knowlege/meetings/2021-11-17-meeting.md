# Nov.17, 2021 at 14:00pm GMT+8

## 1. 议程表
- 知识体系工作组目标讨论（王治文）
- 知识体系工作组共建开发样例进展 （张前福）
- 官网样例展示，样例共建榜及样例规则讨论（马迪欣）

## 2. 出席者
总计33终端接入，主要与会者如下：

- 欧建深
- [zeeman_wang](https://gitee.com/zeeman_wang)
- [zhanglunet](https://gitee.com/zhanglunet)
- [huangsox](https://gitee.com/huangsox)
- [Cruise2019](https://gitee.com/Cruise2019)
- [madixin](https://gitee.com/madixin)
- [kenio_zhang](https://gitee.com/kenio_zhang)
- [laowang-bearpi](https://gitee.com/laowang-bearpi)
- [delphi-tang](https://gitee.com/delphi-tang)
- 李彪（Kid）
- 海阔天空(海阔天空)
- Hao(Hao)
- 赵林华(赵林华)
- 吴非(吴非)
- AAA洪涛(AAA洪涛)
- 软通-王建(软通-王建)
- 田松召(田松召)
- 微笑(九联科技-朱俏威)
- 华秋电子Duke(华秋电子Duke)
- 涂(博通涂文星)
- 颜专(颜专)
- 宁震(宁震)
- 小熊梦工厂(小熊梦工厂)
- 候鹏飞(候鹏飞)
- 杜天微(杜天微)
- 乔楚|HonestQiao(乔楚|HonestQiao)
- 陈道志(victor)
- 裴+(裴嘉@小熊派)
- 九联(九联)
- wenjie(wenjie)
- 睿睿(深开鸿-郭岳峰)
- ulquiorra(ulquiorra)

## 2. 工作进展

+ 深开鸿、润和、小熊派、拓维等厂商输出社区OpenHarmony样例累计76个，部分厂商的Demo正在梳理推社区，此部分持续跟踪；
+ OpenHarmony知识体系工作组样例输出管道已经打通，正在实施基金会OpenHarmony直播计划1期，5个主题，完成1个，4个准备中；2期直播计划征集中；
+ OpenHarmony知识图谱迭代了第二个版本，预计12月份上线；
+ OpenHarmony共建样例排行榜筹备中，评选规则在本次例会进行了表决并通过

## 3. 决议

### 同意OpenHarmony共建样例排行榜的规则

+ 基于openharmony-lts版本
+ 代码上社区
+ 文档支撑开发者可复现
+ 硬件可以公开获取
+ 样例不涉及版权，专利纠纷
+ 难度级别参考知识图谱特性定义（该样例难度取涉及级别最高的一个）
+ 排行榜刷新频率为3个月
+ 排行榜分为个人开发者贡献排行榜和企业贡献排行榜

## 4. 遗留问题

### 知识体系工作组22年度目标确认（王治文）

#### 年度总体目标设定：

建立OpenHarmony知识体系开发者满意度并基线化(6.8)，2022年实现OpenHarmony知识体系开发者满意度提升30%。

#### 目标分解：

1、共建OH全场景Demo  50+，20毕业进主干，10+输送HDC展示、10个拓展成快速体验；OH官网露出OH 30篇深度；11款开发板、30API  Sample、30款Codelab、5本书、30+在线课程、10个成功故事。

2、面向产业界合作伙伴（芯片厂商、OEM厂商、品牌商）的交钥匙技术解决方案、手机APP模板等；---各个参与共建成员

3、挑战面向技术先进性的内容包括但不限于论文、现象级Demo等；---各个参与共建成员

4、小熊派开发板飞天计划;----小熊派

5、参与共建参与度提升：参与的共建单位、个体开发者的提升量化指标确认。----张路

6、参与共建的开发板伙伴的样例规划以及现象级Demo设计，输出线上Demo共建规划表、直播计划。------张前福&&共建伙伴


## 5. 会议回放

[知识体系第二次例会回放](https://meeting.tencent.com/v2/cloud-record/share?id=1aaea540-7011-4fcc-8bec-f83e4ef66001&from=3)

